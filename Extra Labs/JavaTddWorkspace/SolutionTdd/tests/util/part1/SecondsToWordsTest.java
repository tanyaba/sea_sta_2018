package util.part1;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.junit.Test;

// "Getting things set up" test
public class SecondsToWordsTest {
	
	@Test public void convertZeroSeconds() {
		assertThat(SecondsToWords.convert(0), is("0 seconds"));
	}
		
	// Recommended not to use this:
	@Test public void convertZeroSecondsJUnitAssert() {
		assertEquals("0 seconds", SecondsToWords.convert(0));
	}
}
