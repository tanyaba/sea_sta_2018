package stubsandmocks.version3; 

import java.util.Random; 

public class RandomNumberGenerator implements NumberGenerator {

	private Random rand = new Random(); 

	public int generate(int limit) {
		return rand.nextInt(limit)+1; 
	}
}