package stubsandmocks.version2; 

public interface NumberGenerator {
	public int generate(int limit); 
}