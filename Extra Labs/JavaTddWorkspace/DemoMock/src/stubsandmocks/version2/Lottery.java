package stubsandmocks.version2; 

import java.util.HashSet;
import java.util.Set;

/*
 * Refactor number generation strategy to interface
 * 
 * Note: 
 *  1) This is pure Lottery functionality now, i.e. there's no main() method.
 *  2) The methods are instance methods now, not static methods. 
 */
public class Lottery {

	private static final int LOTTERY_SIZE = 6;
	private static final int HIGHEST_NUMBER = 49; 

	// Constructor injection
	private NumberGenerator generator; 

	public Lottery(NumberGenerator generator) {
		this.generator = generator; 
	}

	Set<Integer> generateRandomSet() {
		Set<Integer> numbers = new HashSet<Integer>(LOTTERY_SIZE);
		while (numbers.size() < LOTTERY_SIZE) {
			numbers.add(generator.generate(HIGHEST_NUMBER));
		}
		return numbers;
	}
	
	String formatNumbers(Set<Integer> numbers) {
		int count = 0; 
		StringBuilder sb = new StringBuilder();	
		for (Integer number : numbers) {
			sb.append(number);
			if (++count < numbers.size()) sb.append(" - ");	
		}
		return sb.toString();
	}
}