package stubsandmocks.version3;

import static org.easymock.EasyMock.anyInt;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;

import org.junit.Test;

// This test case illustrates the types of errors/problems you can get when you use EasyMock.
public class LotteryTest_EasyMockFailures {
	
	@Test
	public void gotLessThanIExpected() {
		NumberGenerator mockGenerator = createNiceMock(NumberGenerator.class);
		for (int i = 1; i < 8; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		replay(mockGenerator);
		
		lotto.generateRandomSet();
		verify(mockGenerator);
	}
	
	@Test
	public void gotMoreThanIExpected() {
		NumberGenerator mockGenerator = createMock(NumberGenerator.class);
		for (int i = 1; i < 6; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		replay(mockGenerator);
		
		lotto.generateRandomSet();
		verify(mockGenerator);
	}

	@Test
	public void gotMoreThanIExpectedEvenWithoutVerify() {
		NumberGenerator mockGenerator = createMock(NumberGenerator.class);
		for (int i = 1; i < 6; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		replay(mockGenerator);
		
		lotto.generateRandomSet();
	}

	@Test
	public void didntCallReplay() {
		NumberGenerator mockGenerator = createNiceMock(NumberGenerator.class);
		for (int i = 1; i < 7; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		
		lotto.generateRandomSet();
		verify(mockGenerator);
	}
	
	@Test
	public void gotLessThanIExpectedButDidntVerify() {
		NumberGenerator mockGenerator = createMock(NumberGenerator.class);
		for (int i = 1; i < 8; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		replay(mockGenerator);
		
		lotto.generateRandomSet();
	}
	
	// This is what being nice is all about (in the world of mocks)
	@Test
	public void gotMoreThanIExpectedWithNiceMock() {
		NumberGenerator mockGenerator = createNiceMock(NumberGenerator.class);
		for (int i = 1; i < 6; i++) {
			expect(mockGenerator.generate(anyInt())).andReturn(i);			
		}
		Lottery lotto = new Lottery(mockGenerator);
		replay(mockGenerator);
		
		lotto.generateRandomSet();
	}


	// Another (pre easymock 3): mocking a class w/o using classextensions
}