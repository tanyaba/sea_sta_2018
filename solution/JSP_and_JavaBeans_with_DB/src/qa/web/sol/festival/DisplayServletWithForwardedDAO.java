package qa.web.sol.festival;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @version 	1.0
 * @author Lewis
 */
public class DisplayServletWithForwardedDAO extends HttpServlet {
	private PrintWriter out;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		out = resp.getWriter();

		doData(req, resp);

	}

	public void doData(HttpServletRequest req, HttpServletResponse resp) {

		ServletContext ctx = getServletContext();
		RequestDispatcher rd = null;
		String pageName = null;

		int day = 0;
		String dayString = req.getParameter("day");
		try {
			day = Integer.parseInt(dayString);
			PlayBean pb = new PlayBean();
			pb.setDay(day);
			if (pb.getResultsObtained()) {
				pageName = "/results";
				req.setAttribute("pb", pb);
			} else {
				pageName = "/nodata";
			}
		} catch (Exception e) {
			e.printStackTrace();
			pageName = "/nodata";
		}

		try {
			rd = ctx.getRequestDispatcher(pageName);
			rd.forward(req, resp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}




}
