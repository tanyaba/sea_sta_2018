package qa.java.sol.hibernate;
import javax.persistence.*;


@Entity
@Table(name="bank_transactions")
public class BankTransaction implements java.io.Serializable {
	
	@Id
	@Column(name="trans_no")
	private int id;
	
	@Column(name="trans_date")	
	private java.util.Date date;
	@Column(name="trans_amount")	
	private double amount;
	@Column(name="debit_credit")	
	private String debitOrCredit;
	@Column(name="account_no")	
	private int accountNumber;

	// every transaction links to a BankAccount
	// we store the account object's reference
	// ...plus of course the account_no var is the Foreign Key
	@ManyToOne
	@JoinColumn(name="account_no", nullable=false, insertable=false, updatable=false)
	private BankAccount bankAccount;
	
	
	public BankAccount getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(BankAccount bankAccount) {
		this.bankAccount = bankAccount;
	}

	public BankTransaction() { }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDebitOrCredit() {
		return debitOrCredit;
	}

	public void setDebitOrCredit(String debitOrCredit) {
		this.debitOrCredit = debitOrCredit;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	
	

}
