/*
 *
 * Student
 *
 * A student has a name and age inherited from
 * Person and a subject all of its own.
 *
 */

package qa.java.sol.person2;

public class Student extends Person
{
	// Additional instance variable for student's subject
	private String subject;

	// Constructor
	//
	public Student(String n, short a, String sub)
	{
		super(n,a);  // first construct superclass explicitly
		subject = sub;
	}


	//
	// Override getDetails() instance method of superclass to return
	// full details of student (age and subject) in a string, e.g.,
	// "32 + ", " + "Physics" (but, of course, not hardcoded like this!)
	//
	// Note that this method should not attempt to access
	// instance variables of superclass directly, because
	// (a) it is bad practice
	// (b) variables may be private to superclass
	//
	public String getDetails()
	{
		return super.getDetails() + ", " + subject;
	}


	//
	// Mutator method to change student's subject
	//
	public void changeSubject(String newsub)
	{
		subject = newsub;
	}

}