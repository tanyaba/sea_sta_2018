package qa.java.sol.calculator;

import java.awt.*;
import java.awt.event.*;

public class Calculator1 extends Frame
{
	private TextField display;
	private String[] keyLabels = {
									"7", "8", "9", "*",
									"4", "5", "6", "/",
									"1", "2", "3", "-",
									"0", ".", "=", "+"
								 };
    private Button[] key;
	private boolean firstDigit = true;
	private double scratchpad = 0.0;
	private char operationChar = '=';

	private CheckboxMenuItem twoDPCheckboxMenuItem = null;
	private boolean twoDecPlaces = false;


	public Calculator1(String title)
	{
		super(title);

		setLayout(new BorderLayout(0, 10));
		display = new TextField("0");
		display.setEditable(false);
		display.setFont(new Font("Helvetica", Font.PLAIN, 20));
		add("North", display);

		Panel keyPanel = new Panel();
		keyPanel.setLayout(new GridLayout(4, 4, 5, 5));

		key = new Button[keyLabels.length];
		for (int i = 0; i < keyLabels.length; i++)
		{
		    key[i] = new Button(keyLabels[i]);
			keyPanel.add(key[i]);
		}
		add("Center", keyPanel);

        setUpListeners();

		setSize(200, 200);
		show();

}


	//
	// Override getInsets() method to inset all components
	// by 10 pixels from left, right and bottom edges of
	// container, and 50 pixels from top edge (to allow
	// for combined height of title bar and menu bar).
	//

	public Insets getInsets()
	{
		return new Insets(50, 10, 10, 10);
	}


    //
    //  define a class which implments ActionListener for entry keys
    //
    private class EntryListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Append character to those displayed.
		// If it's entered immediately after an
		// operator key, clear display first.
            Object source = evt.getSource();
            if(source instanceof Button)
            {
			    String s = ((Button)source).getLabel();
				if (firstDigit)
				{
					display.setText(s);
					firstDigit = false;
				}
				else
					display.setText(display.getText() + s);
			}
        }
    }

    //
    //  define a class which implments ActionListener for operation keys
    //
    private class OperationListener implements ActionListener
    {
        public void actionPerformed(ActionEvent evt)
        {
		// Save operator and contents of display (as a double);
            Object source = evt.getSource();
            if(source instanceof Button)
            {
			    String s = ((Button)source).getLabel();
    			char ch = s.charAt(0);
				// "+", "-", "*" or "/" key
				operationChar = ch;
				scratchpad = new Double(display.getText()).doubleValue();
				firstDigit = true;
			}
        }
    }


    //
    //  define a class which implments ActionListener for the calculation key
    //
    private class CalculationListener implements ActionListener
    {
        public CalculationListener()
        {
        }
        public void actionPerformed(ActionEvent evt)
        {
		// If this was not entered
		// immediately after any operator, do the necessary
		// calculation and display the result
			if (!firstDigit)
			{
				doCalculation(new Double(display.getText()).doubleValue());
				if (twoDecPlaces)
					// round number up/down to 2 decimal places
					scratchpad = Math.round(scratchpad * 100)/100.0;
				display.setText("" + scratchpad);
				firstDigit = true;
			}

        }
    };

	//
	// setUpListeners{} method creates and registers
	// listeners for each key and for the dialog
	//

	public void setUpListeners()
	{
	    ActionListener entryListener = new EntryListener();
	    ActionListener operationListener = new OperationListener();
	    ActionListener calculationListener = new CalculationListener();


	    //  for each key, create and regsiter the apporpriate type of listener
        for (int keyIndex = 0; keyIndex<keyLabels.length; ++keyIndex)
        {
            char ch = keyLabels[keyIndex].charAt(0);
			// If it's a number or the decimal point, register an EntryListener
			if (ch >= '0' && ch <= '9' || ch == '.')
			{
			    key[keyIndex].addActionListener(entryListener);
			}

			// If it's any operator apart from "=",  register an OperationListener
			else if (ch != '=')
			{
				// "+", "-", "*" or "/" key
			    key[keyIndex].addActionListener(operationListener);
			}

			// If it's the "=" operator, register a CalculationListener
			else
			{
				// "=" key
			    key[keyIndex].addActionListener(calculationListener);
			}
		}


		//  create and register a Listener to shut down when the window closes

		 WindowListener wl = new WindowAdapter()
                		    {
                		        public void windowClosing(WindowEvent evt)
                		        {
                		            dispose();
                		            System.exit(0);
                		        }
                		    };
		addWindowListener(wl);
	}

	//
	// Do the calculation
	//

	private double doCalculation(double operand)
	{

		switch (operationChar)
		{
			case '+':
				scratchpad += operand;
				break;
			case '-':
				scratchpad -= operand;
				break;
			case '*':
				scratchpad *= operand;
				break;
			case '/':
				scratchpad /= operand;
				break;
			case '=':
				scratchpad = operand;
				break;
		}
		return scratchpad;
	}

	//
	// Entry point
	//

	public static void main(String[] args)
	{
		Calculator1 cal = new Calculator1("Calculator");
	}

}