/* SortTest is a Java application for testing the Sort class */
package qa.java.sol.sort;

public class SortTest
{
	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		// Create an array of ten integers
		//
		int[] myArray = new int[10];

		// Initialise the array with random values
		// (using random() class method of Math class)
		//
		for (int i = 0; i < myArray.length; i++)
			myArray[i] = (int)(100*Math.random());

		// Print out contents of original array
		//
		System.out.println("\nOriginal array is:\n");
		for (int i = 0; i < myArray.length; i++)
			System.out.print(myArray[i] + " ");
		System.out.println();

		// Sort array using class method of Sort class
		//
		Sort.bSort(myArray);

		// Print out contents of sorted array
		//
		System.out.println("\nSorted array is:\n");
		for (int i = 0; i < myArray.length; i++)
			System.out.print(myArray[i] + " ");
		System.out.println();

	}

}