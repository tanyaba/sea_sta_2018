
$(document).ready(function(){
	
	// TO DO:
	//=======
	
	// Each <h2> element names a martial art. Use a selector to pick up all three descriptive paragraphs that 
	// follow <h2>s. Ensure that all three paragraphs are initially hidden when the user sees the page.
	
	
	
	
	
	
	
	
	
	// TO DO:
	//=======
	
	// Use a click-event handler to ensure that whenever an <h2> element is clicked,
	// we display the following paragraph if it was already hidden. If the paragraph 
	// was already visible, then hide it when the mouse is clicked on the <h2>. In other 
	// words, we alternate between hide and revealing the paragraph.
	
	// Specify a one-second duration (or longer) so that the element doesn't just appear or 
	// disappear suddenly.
	
	
	
	
	
	
	
	// TO DO:
	//=======
	
	// Use two mouse-event handlers for <h2> elements: when the mouse pointer moves over an 
	// <h2>, then we want its colour to change (your choice!); when the pointer moves away, 
	// we want the <h2> to revert to its original colour.
	

	// TO DO:
	//=======
	
	//Just for the practice...! Comment out the function that hides or reveals 
	//the paragraphs following <h2> elements. Re-implement it using a Boolean variable
	//that indicates whether the paragraph is currently visible or invisible.
	
	//Instead of using toggle or fade, this time we want the elements to slide 
	//vertically in and out of view.
	
	
	
	// TO DO:
	//=======
	
	// We want to show the table with columns of alternating colours.
	
	
	// Lighten the background colour of alternating columns in table. Keep the other 
	// columns as they were, with the same background colour as the <div> that the table is in.
	
	// HINT: you can achieve this by altering the colour of every other table-data cell.
	
	// (This technique gives alternating columns only if the number of columns is even;
	// with an odd number of columns, this technique results in a chessboard effect.)
	//
	

	// TO DO:
	//=======
	
	// Change the background colour of a row while it's being pointed at
	//
	
	// We've already done this for a different element type. If you didn't use the hover( )
	// function before, then use it now. 
	
	// ... And if you did use it before, feel free to use mouseover and mouseout this time!
	
	
	
	
 	
});