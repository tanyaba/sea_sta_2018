package qa.web.lab.sessions;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @author lewis
 * 
 */
public class MainServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			
		resp.setContentType("text/html");

		String body = null;

		String path = req.getServletPath();
		
		System.out.println("MainServlet with Servlet Path: " + path);

		/*
		 * QA To do: Exercises 1 step 5, 2 step 1, 3 step 2
		 * 
		 * Provide conditional code to assign the 'body' variable
		 * a suitable value based on the 'path' of the servlet's
		 * invocation, e.g.
		 * 
		 * 		if (path.equals("/login")) body = "/login.jsp";
		 */
		

		/*
		 * QA To do: Exercise 1 step 4
		 * 
		 * Using the ServletContext, get a RequestDispatcher and
		 * forward to the header JSP, the body JSP and the footer
		 * JSP in turn, to build up the complete response. 
		 */

	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {
			doGet(req, resp);
	}

}
