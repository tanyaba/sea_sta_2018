package qa.web.lab.festival;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @version 	1.0
 * @author Lewis
 */
public class DisplayServletWithForwardedDAO extends HttpServlet {
	private PrintWriter out;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		out = resp.getWriter();

		doData(req, resp);

	}

	public void doData(HttpServletRequest req, HttpServletResponse resp) {

		ServletContext ctx = getServletContext();
		RequestDispatcher rd = null;
		String pageName = null;

		int day = 0;
		String dayString = req.getParameter("day");
		try {
			day = Integer.parseInt(dayString);

			/*
			 * QA To do: exercise 2 step 9
			 *
			 * Create a PlayBean, and set the relevant property.
			 * Recall the resultsObtained flag in the bean code,
			 * and associated getResultsObtained() method, decide
			 * which page to forward the request to.
			 *
			 * How will you forward the bean to the results page?
			 * Do you need to make any changes to the JSPs?
			 */


		} catch (Exception e) {
			e.printStackTrace();
			pageName = "/nodata";
		}

		try {
			rd = ctx.getRequestDispatcher(pageName);
			rd.forward(req, resp);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}




}
